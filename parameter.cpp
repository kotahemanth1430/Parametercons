#include<iostream>
using namespace std;
class Student
{
    public:
    string collname;
    int college;
    string Name;
    double sem;
    Student() //default constructor
    {
        collname="MVGR";
        college=97;
    }
    Student(Student &obj) // copy constructor
    {
        cout<<"name: \t";
        cin>>Name;
        cout<<"sem percentage: \t";
        cin>>sem;
        collname=obj.collname;
        college=obj.college;
    }
    void display()
    {
        cout<<"college name: "<<collname<<endl;
        cout<<"code: "<<college<<endl;
        cout<<"Name: "<<Name<<endl;
        cout<<"sem: "<<sem<<endl;
    }
    ~Student()
    {
        cout<<"DEAD"<<endl;
    }
    Student(string fullname,double semper) //parameter constructor
    {
      Name=fullname;
      sem=semper;
    }
};
int main()
{
    Student obj1;
    Student obj2=obj1;
    cout<<"full name: \t";
    cout<<obj2.Name<<endl;
    cout<<"sem percentage: \t";
    cout<<obj2.sem<<endl;
    obj2.display();

}